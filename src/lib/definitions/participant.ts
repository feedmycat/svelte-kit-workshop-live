export type Participant = {
	name: string;
	lastName: string;
	gitlab: string;
	kaggle: string;
	regDate: string;
};

export type ParticipantPG = {
	Name: string;
	LastName: string;
	Email: string;
	GitlabUsername: string;
	KaggleId: string;
};
